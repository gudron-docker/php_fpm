#!/bin/bash
set -e

ansible-playbook /opt/ansible/basic/playbook.yml

ln -sf /dev/stderr /var/log/php-fpm.log

exec /usr/sbin/php-fpm5.6 -c /etc/php/5.6/fpm/