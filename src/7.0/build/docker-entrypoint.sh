#!/bin/bash
set -e

ansible-playbook /opt/ansible/basic/playbook.yml

ln -sf /dev/stderr /var/log/php-fpm.log

exec /usr/sbin/php-fpm7.0 -c /etc/php/7.0/fpm/